import { Component } from "@angular/core";

import { InputNumber } from "../number";

@Component({
    selector: 's-input-fancy-number',
    template: `<marquee><s-input-number></s-input-number></marquee>`
})
export class InputFancyNumber {

}

export const dependencies = [InputFancyNumber, InputNumber];
