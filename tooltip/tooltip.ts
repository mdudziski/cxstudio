import { Component } from "@angular/core";

@Component({
    selector: 's-tooltip',
    template: 'I am the tooltip'
})
export class Tooltip {

}

export const dependencies = [Tooltip];
